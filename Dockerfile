FROM python:3.7-buster
LABEL maintainer "Aaron Schmid <aaron.schmid@outlook.com>"

WORKDIR /app/backend

# install all dependencies before moving code
# to make sure docker layer caching is used
COPY requirements.txt .
RUN pip3 install -r requirements.txt

COPY . .

# remove the local config and replace it with
# the one that should be used in a docker env
RUN rm ./config.ini &&\
    mv ./config.docker.ini ./config.ini

RUN chmod +x ./scripts/*

# install misaki as module
# currently if we install our module via setuptools
# it messes up our config files
# RUN pip3 install .
# so instead we just gonna add the module directory to the path
# python will now find our module when we try to import it
ENV PYTHONPATH ${PYTHONPATH}:/app/backend/:/app/backend/service:/app/backend/model

ENTRYPOINT exec /app/backend/scripts/backend-start --detect-seed
