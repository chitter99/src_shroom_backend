from dependency_injector import containers, providers
from redis import Redis
import mongoengine

from backend.config import config
from backend.service import Misaki
from backend.service import Utils
from backend.web import Web
from backend.seed import seed


def main(db, web):
    """
    Starts the backend. We don't use db parameter but still need it to ensure that the dj is connecting to the db.
    :param db: mongodb connection
    :param web: web instance
    """
    web.run()


class BackendContainer(containers.DeclarativeContainer):
    """ Backend container which holds all dj providers """

    config = providers.Configuration('config')
    """ Config provider which will be provided via config module when the container is instantiated """

    utils = providers.Singleton(Utils, config=config)

    redis = providers.Singleton(Redis, host=config.redis.host, port=config.redis.port, db=config.redis.db)
    db = providers.Singleton(mongoengine.connect, host=config.mongo.db)
    misaki = providers.Singleton(Misaki, redis=redis, queue_name=config.redis.queue)
    web = providers.Singleton(Web, config=config, misaki=misaki, utils=utils)

    seed = providers.Callable(seed, db=db, cnf=config, utils=utils)
    """ Function to seed the database """
    main = providers.Callable(main, db=db, web=web)


def bootstrap() -> BackendContainer:
    """
    Instantiates the container and assigns the correct config file
    :return BackendContainer
    """
    container = BackendContainer(config=config)
    return container
