import argparse

from backend.backend import bootstrap, BackendContainer
from backend.model import Mushroom, MushroomImage


def is_seeding_required(container: BackendContainer):
    """
    Check the database if seeding is required
    :param BackendContainer container: instance of the container
    :return bool
    """

    container.db()  # make sure db is in correct state
    if Mushroom.objects.count() == 0 and MushroomImage.objects.count() == 0:
        # database seams empty
        return True
    return False


def cli():
    """ Starts the cli pipeline """
    args = parser.parse_args()
    container = bootstrap()

    if args.seed_on_start or (args.detect_seed and is_seeding_required(container)):
        container.seed()
    else:
        print('[INFO] Not seeding db')

    container.main()


parser = argparse.ArgumentParser(description='configures and starts the backend')
parser.add_argument('--seed-on-start', action='store_true')
parser.add_argument('--detect-seed', action='store_true')

if __name__ == '__main__':
    cli()
