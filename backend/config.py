import configparser
import os

from backend.service.utils import string_to_bool


class AttrDict(dict):
    """ Dictionary that allows it's values to be read as attributes """
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self


def _string_to_bool(string) -> bool:
    """
    Converts string to bool
    :param string: string to convert
    :return bool
    """
    if string == 'True':
        return True
    return False


def _translate_path(path) -> str:
    """
    Translates a relative path inside the config.ini to a absolute path
    :param str path: path in config.ini (e.g. volume/latest.h5)
    :return: str
    """
    return os.path.abspath(os.path.join(os.path.dirname(__file__), '..', path))


def load_config(filename='config.ini') -> AttrDict:
    """
    Loads the given config
    :param str filename: relative path of config
    :return AttrDict
    """
    conf = configparser.ConfigParser()
    conf.read(_translate_path(filename))
    return AttrDict({
        'backend': AttrDict({
            'static': _translate_path(conf.get('backend', 'static')),
            'port': conf.get('backend', 'port'),
            'host': conf.get('backend', 'host'),
            'save_uploads': string_to_bool(conf.get('backend', 'save_uploads')),
            'save_uploads_to': _translate_path(conf.get('backend', 'save_uploads_to'))
        }),
        'redis': AttrDict({
            'host': conf.get('redis', 'host'),
            'port': conf.get('redis', 'port'),
            'db': conf.get('redis', 'db'),
            'queue': conf.get('redis', 'queue'),
        }),
        'mongo': AttrDict({
            'db': conf.get('mongo', 'db')
        }),
        'seed': AttrDict({
            'mushrooms': _translate_path(conf.get('seed', 'mushrooms')),
            'mushroomimages': _translate_path(conf.get('seed', 'mushroomimages'))
        })
    })


config = load_config()

