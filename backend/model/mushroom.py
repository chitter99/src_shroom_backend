import datetime
from mongoengine import *


class Mushroom(Document):
    """ Mushroom entity is a type of mushroom our app knows """

    name = StringField(required=True, unique=True)
    """ Latin name of the mushroom """
    title = StringField(required=True)
    """ German display name of the mushroom """
    description = StringField(required=True, default='')
    """ German description of the mushroom """
    poisonous = BooleanField(required=True, default=False)
    """ Is the mushroom poisonous? """
    edible = BooleanField(required=True, default=False)
    """ Can we eat the mushroom? """
    thumbnail_url = StringField(required=True, default='')
    """ Thumbnail url for mushroom """
    created_at = DateTimeField()
    """ Creation time of the entity """

    def save(self, *args, **kwargs):
        if not self.created_at:
            self.created_at = datetime.datetime.now()
        return super(Mushroom, self).save(*args, **kwargs)

    def to_dto(self):
        """ Converts entity to it's data transfer object """
        return {
            'name': self.name,
            'title': self.title,
            'description': self.description,
            'poisonous': self.poisonous,
            'edible': self.edible,
            'thumbnailUrl': self.thumbnail_url,
            'createdAt': str(self.created_at)
        }


class MushroomImage(Document):
    """ MushroomImage entity is a image for a mushroom. One mushroom can have multiple images. """
    url = StringField(required=True)
    """ Url of the mushroom image """
    mushroom = ReferenceField(required=True, document_type=Mushroom)
    """ Reference to the mushroom """
    order = IntField(required=True, default=10)
    """ How should we prioritize this image in regards to the other? Smaller is higher. """
    created_at = DateTimeField()
    """ Creation time of the entity """

    def save(self, *args, **kwargs):
        if not self.created_at:
            self.created_at = datetime.datetime.now()
        return super(MushroomImage, self).save(*args, **kwargs)

    def to_dto(self):
        """ Converts entity to it's data transfer object """
        return {
            'url': self.url,
            'mushroom': self.mushroom.name,
            'order': self.order,
            'createdAt': str(self.created_at)
        }
