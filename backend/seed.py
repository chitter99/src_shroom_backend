import csv
import os

from backend.model import Mushroom, MushroomImage
from backend.service.utils import string_to_bool, get_files_in_directory


def seed(db, cnf, utils):
    """
    Seeds the database with data defined in seed csv
    :param db: mongodb instance
    :param cnf: config values
    :param utils: utils instance
    """
    print('[INFO] Started seeding...')
    with open(cnf.seed.mushrooms) as file:
        reader = csv.reader(file)
        next(reader)
        for row in reader:
            mushroom = Mushroom(name=row[0], title=row[1], description=row[2],
                                poisonous=string_to_bool(row[3]), edible=string_to_bool(row[4]),
                                thumbnail_url=cnf.backend.host + '/api/mushroom/{}/thumbnail'.format(row[0]))
            mushroom.save()

            images_directory = os.path.join(cnf.seed.mushroomimages, mushroom.name)
            order = 10
            for image in get_files_in_directory(images_directory):
                image_path = os.path.join(images_directory, image)
                mushroom_image = MushroomImage(url=utils.get_public_path(image_path), mushroom=mushroom, order=order)
                mushroom_image.save()
                order += 10
    print('[INFO] finished seeding!')
