import asyncio
import base64

from rq import Queue


class PredictionFailedException(Exception):
    """ Prediction could not be finished """
    pass


class Misaki:
    """ Service for communicating with Misaki """
    def __init__(self, redis, queue_name='misaki', queue_func='misaki.predict_from_base64'):
        """
        :param redis: redis client
        :param str queue_name: name of misaki's queue
        :param str queue_func: which func should we use for prediction
        """
        self._redis = redis
        self._queue = Queue(queue_name, connection=self._redis)
        self._queue_func = queue_func

    async def predict(self, image_data, timeout=10000):
        """
        Sends image data to misaki and waits for a prediction
        :param image_data: raw input image data
        :param int timeout: how long should we wait for misaki?
        :return array
        """
        return await asyncio.wait_for(asyncio.shield(self._work(image_data)), timeout=timeout)

    def _enqueue_work(self, image_data):
        """
        Encodes raw image data to base64 and adds it to the misaki queue
        :param image_data: raw image data
        :return Union
        """
        encoded = base64.b64encode(image_data)
        return self._queue.enqueue(self._queue_func, args=(encoded,))

    async def _work(self, image_data):
        """
        Enqueue job and wait for result
        :param image_data: raw image data
        :return: array
        """
        job = self._enqueue_work(image_data)
        while not job.is_failed:
            await asyncio.sleep(1 / 1000)
            if job.is_finished:
                return job.result
        raise PredictionFailedException()
