import os


def string_to_bool(string) -> bool:
    """
    Converts string to bool
    :param string: string to convert
    :return bool
    """
    if string == 'True':
        return True
    return False


def get_files_in_directory(directory):
    """
    Shortcut to list files in directory
    :param str directory: directory to look into
    :return array
    """
    return [f for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f))]


class Utils:
    """ Utils service for functions which affect other services """
    def __init__(self, config):
        self._config = config

    def get_public_path(self, asset) -> str:
        """
        Returns the public url of a asset (e.g. /assets/icon.ico)
        :param str asset: absolute path of asset
        :return str: public url
        """
        return os.path.relpath(asset, self._config.backend.static)

    def get_asset_path(self, public) -> str:
        """
        Returns the absolute path of an public url
        :param str public: public url
        :return str: absolute path
        """
        return os.path.abspath(os.path.join(self._config.backend.static, public))
