import io
import os
import uuid

from aiohttp import web
from mongoengine import DoesNotExist
from PIL import Image

from backend.model import Mushroom, MushroomImage
from backend.service.misaki import PredictionFailedException
from backend.service import Utils, Misaki


class Web:
    """ Backend http handler class """
    def __init__(self, config, misaki: Misaki, utils: Utils):
        # we have to increase client_max_size to support uploading images up to 10mb
        self.app = web.Application(client_max_size=1024**10)
        self._config = config
        self._misaki = misaki
        self._utils = utils
        self._setup()

    def _setup(self):
        """ Sets up all routes """
        self.app.add_routes([
            web.put('/api/model/misaki/predict', self.handle_prediction),
            web.get('/api/mushrooms', self.handle_get_mushrooms),
            web.get('/api/mushroom/{mushroomName}', self.handle_get_mushroom),
            web.get('/api/mushroom/{mushroomName}/images', self.handle_get_mushroom_images),
            web.get('/api/mushroom/{mushroomName}/thumbnail', self.handle_get_mushroom_thumbnail)
        ])
        self.app.router.add_static('/', path=self._config.backend.static, name='static')

    def run(self):
        """ Starts the backend server """
        web.run_app(self.app, port=self._config.backend.port)

    async def handle_prediction(self, req: web.Request):
        """ Handles prediction requests """
        post = await req.post()

        post_image = post.get('image')
        if not post_image:
            return web.json_response({
                'status': 'error',
                'reason': 'missing_image'
            })

        image_data = post_image.file.read()

        if self._config.backend.save_uploads:
            self.save_upload(image_data)

        try:
            result = await self._misaki.predict(image_data)
        except PredictionFailedException:
            return web.json_response({
                'status': 'error',
                'reason': 'prediction_failed'
            })

        return web.json_response({
            'status': 'ok',
            'data': {
                'prediction': result[0]
            }
        })

    async def handle_get_mushrooms(self, req: web.Request):
        """ Handles get mushrooms """
        mushrooms = []
        for mushroom in Mushroom.objects():
            mushrooms.append(mushroom.to_dto())
        return web.json_response({
            'status': 'ok',
            'data': {
                'mushrooms': mushrooms
            }
        })

    async def handle_get_mushroom(self, req: web.Request):
        """ Handles get mushroom """
        try:
            mushroom = Mushroom.objects.get(name=req.match_info['mushroomName'])
        except DoesNotExist:
            return web.Response(status=404, text='Could not find mushroom')
        return web.json_response({
            'status': 'ok',
            'data': {
                'mushroom': mushroom.to_dto()
            }
        })

    async def handle_get_mushroom_images(self, req: web.Request):
        """ Handles get mushroom images """
        try:
            mushroom = Mushroom.objects.get(name=req.match_info['mushroomName'])
        except DoesNotExist:
            return web.Response(status=404, text='Could not find mushroom')

        images = []
        for mushroomImage in MushroomImage.objects(mushroom=mushroom).order_by('order'):
            images.append(mushroomImage.to_dto())

        return web.json_response({
            'status': 'ok',
            'data': {
                'images': images
            }
        })

    async def handle_get_mushroom_thumbnail(self, req: web.Request):
        """ Handles get thumbnail for mushroom, returns the thumbnail as file """
        try:
            mushroom = Mushroom.objects.get(name=req.match_info['mushroomName'])
        except DoesNotExist:
            return web.Response(status=404, text='Could not find mushroom')

        images = MushroomImage.objects(mushroom=mushroom).order_by('order')
        if len(images) < 1:
            return web.Response(status=404, text='Could not find image for mushroom')
        return web.FileResponse(self._utils.get_asset_path(images[0].url))

    def save_upload(self, data: bytes):
        """
        Saves uploaded data to the in the config defined path
        :param bytes data: data to save
        """
        path = os.path.join(self._config.backend.save_uploads_to, str(uuid.uuid4()) + '.jpg')
        buf = io.BytesIO(data)
        image = Image.open(buf)
        image.save(path)
        print('[INFO] Saved uploaded image to {}'.format(path))
