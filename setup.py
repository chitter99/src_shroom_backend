from setuptools import setup


# read requirements file as the required imports are defined their
with open('./requirements.txt') as fp:
    install_requires = fp.read()

setup(
    name='backend',
    version='1.0',
    descirption='Backend for the cool mushroom app.',
    author='Aaron Schmid',
    author_email='aaron.schmid@outlook.com',
    url='https://whatmushroom.app/',
    install_requires=install_requires,
    scripts=[
        './scripts/start.py'
    ]
)
